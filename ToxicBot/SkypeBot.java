package io.github.toxicbyte.SkypeBot;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.skype.*;
public class SkypeBot
{
	private boolean afk;
	private static Set<String> admins;
	@SuppressWarnings("unused")
	private static File data;
	private String afkReason;
	private String admin = "Martin Carrasco"; //YourName here
	private static JPanel panel;
	public static void main(String[] args) throws SkypeException, IOException
	{
		panel = new JPanel();
		JFrame frame = new JFrame("Closer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);
		frame.setVisible(true);
		data = new File("data.dat");
		Skype.addChatMessageListener(new skypeChatListener());
		Skype.setDaemon(false);
	}
	public boolean getAFK()
	{
		if (afk)
		{
			return true;
		}
		return false;
	}
	public String getAFKReason()
	{
		return afkReason;
	}
	public SkypeBot()
	{
		
	}
	public Set<String> getAdmins()
	{
		return admins;
	}
	public void setAFK(Boolean choice, String reason)
	{
		afk = choice;
		afkReason = reason;
	}
	public String getAdminName()
	{
		return admin;
	}
}
