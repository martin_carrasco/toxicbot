package io.github.toxicbyte.SkypeBot;

import java.io.File;
import java.util.Random;

import javax.swing.JOptionPane;

import com.skype.ChatMessage;
import com.skype.ChatMessageListener;
import com.skype.Friend;
import com.skype.Profile;
import com.skype.Profile.Status;
import com.skype.Skype;
import com.skype.SkypeException;

public class skypeChatListener implements ChatMessageListener
{
	private String lastMsg = "";
	private SkypeBot bot = new SkypeBot();
	@Override
	public void chatMessageReceived(ChatMessage msg) throws SkypeException
	{
		if (bot.getAFK())
		{
			msg.getChat().send("[" + bot.getAdminName() + "]" + bot.getAFKReason());
		}
	}

	@Override
	public void chatMessageSent(ChatMessage msg) throws SkypeException
	{
		int n = 0;
		int times = 0;
		String message;
		String msgToSend = "";
		String msgToSpam = "";
		message = msg.getContent();
		if (message.startsWith("!"))
		{
			String [] args = message.split("\\s+");
			switch(args[0].toLowerCase())
			{
			case"!msg":
				if (args.length > 3)
				{
					msgToSend = "";
					for (String key : args)
					{
						if (key.equalsIgnoreCase(args[0]) || key.equalsIgnoreCase(args[1]))
						{
							continue;
						}
						msgToSend = msgToSend + " " + key;
					}
					Friend friend = Skype.getContactList().getFriend(args[1]);
					friend.send(msgToSend);
					break;
				}
				msg.getChat().send("Correct usage = !msg <name> <msg>");
				break;
			case"!spam":
				if (args.length < 2)
				{
					msg.getChat().send("You didnt provide enough arguments!");
					break;
				}
				for (String key : args)
				{
					if (key.equals(args[0]) || key.equals(args[1]))
					{
						continue;
					}
					msgToSpam = msgToSpam + " " + key;
				}
				System.out.println(msgToSpam);
				times = Integer.parseInt(args[1]);
				while(n < times)
				{
					msg.getChat().send(msgToSpam);
					n += 1;
				}
				break;
			case"!help":
				break;
			case"!hop":
				Random rand = new Random();
				int randomInt = rand.nextInt((5 - 0) + 1) + 0;
				switch(randomInt)
				{
				case 0:
					msg.getChat().send("\"Hip Hop's dead and I'm the lucky savior\"");
					break;
				case 1:
					msg.getChat().send("\"Motherfucker you're not dope, so you try to get some attention by cussing and eating a fucking cockroach?\"");
					break;
				case 2:
					msg.getChat().send("\"Hop can probably make 'em bounce like a trampoline\"");
					break;
				case 3:
					msg.getChat().send("\"The dark knight's in this bitch, without the mask and cape\"");
					break;
				case 4:
					msg.getChat().send("\"You think you original? You ain't original, you were programmed to be you,"
							+ "you're a slave and you don't even know it, the shit you think of ain't even your thoughts,"
							+"let's step out the box for a minute\"");
					break;
				case 5:
					msg.getChat().send("\"Real artists get shelved and wack one's get famous\"");
					break;
				case 6:
					msg.getChat().send("\"Someone tell this madafakas that Hop is back\"");
					break;
				default:
					break;
				}
				break;
			case"!quote":
				Random rand2 = new Random();
				int randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
				switch(randomInt2)
				{
				case 0:
					if (lastMsg.equals("\"Gigolo blood line, pimpin' hereditary\""))
					{
						randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
					}
					msg.getChat().send("\"Gigolo blood line, pimpin' hereditary\"");
					lastMsg = "\"Gigolo blood line, pimpin' hereditary\"";
					break;
				case 1:
					if (lastMsg.equals("\"Started from the bottom now we here\""))
					{
						randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
					}
					msg.getChat().send("\"Started from the bottom now we here\"");
					lastMsg = "\"Started from the bottom now we here\"";
					break;
				case 2:
					if (lastMsg.equals("\"I'm in the club high on purp with my J's on\""))
					{
						randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
					}
					msg.getChat().send("\"I'm in the club high on purp with my J's on\"");
					lastMsg = "\"I'm in the club high on purp with my J's on\"";
					break;
				case 3:
					if (lastMsg.equals("\"Old men have heart attacks and i don't wanna be responsable for that\""))
					{
						randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
					}
					msg.getChat().send("\"Old men have heart attacks and i don't wanna be responsable for that\"");
					lastMsg  = "\"Old men have heart attacks and i don't wanna be responsable for that\"";
					break;
				case 4:
					if (lastMsg.equals("\"Money, Hoes, Money and Hoes\""))
					{
						randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
					}
					msg.getChat().send("\"Money, Hoes, Money and Hoes\"");
					lastMsg = "\"Money, Hoes, Money and Hoes\"";
					break;
				case 5:
					if (lastMsg.equals("\"Gigolo blood line, pimpin' hereditary\""))
					{
						randomInt2 = rand2.nextInt((5 - 0) + 1) + 0;
					}
					msg.getChat().send("\"Gigolo blood line, pimpin' hereditary\"");
					lastMsg = "\"Gigolo blood line, pimpin' hereditary\"";
					break;
				}
				
				break;
			case"!afk":
				if (args.length == 1)
				{
					if (bot.getAFK())
					{
						bot.setAFK(false, "");
						msg.getChat().send("AFK OFF!");
						break;
					}
					msg.getChat().send("To turn on AFK provide a reason as argument");
					break;
				}
				else
				{
					if (!bot.getAFK())
					{
						String reason = "";
						for (String key : args)
						{
							if (key.equalsIgnoreCase(args[0]))
							{
								
							}
							reason = reason + " " + key;
							
						}
						bot.setAFK(true, reason);
						msg.getChat().send("AFK ON!");
						break;
					}
					msg.getChat().send("Too many arguments");
					break;
				}
			case "!mc":
				File fileMc = new File("C:\\Users\\Martin\\Desktop");
				for (File files : fileMc.listFiles())
				{
					if (files.getName().equalsIgnoreCase("Minecraft"))
					{
							try 
							{
								Runtime.getRuntime().exec("C:\\Users\\Martin\\Desktop\\Minecraft.exe");
							} catch (Exception e) 
							{
								JOptionPane.showMessageDialog(null, "Exception: " + e.getMessage());
							}
							Profile prof = Skype.getProfile();
							prof.setStatus(Status.DND);
							break;
					}
				}
				JOptionPane.showMessageDialog(null, "No Minecraft.exe found in the desktop!");
				break;
			case "!lol":
				File fileLol = new File("C:\\Users\\Martin\\Desktop");
				Profile prof = null;
				for (File files : fileLol.listFiles())
				{
					if (files.getName().equalsIgnoreCase("League of Legends.lnk"))
					{
						try 
						{
							new ProcessBuilder("cmd", "/c", "C:\\Users\\Martin\\Desktop\\" + files.getName());
						} catch (Exception e) 
						{
							JOptionPane.showMessageDialog(null, "Exception: " + e.getMessage());
						}
						prof = Skype.getProfile();
						prof.setStatus(Status.DND);
						break;
					}
				}
				if (prof.getStatus() != Status.DND)
				{
					JOptionPane.showMessageDialog(null, "No League of Legends.exe found in the desktop!");
					break;
				}
				break;
			default:
				msg.getChat().send("Use !help for all commands!");
				break;
			}
		}
		
	}

}
